#!/bin/bash

export LISP_PATH="$LANG_PATH/dist"

################################################################################

ronin_require_lisp () {
    echo hello world >/dev/null
}

ronin_include_lisp () {
    motd_text "    -> Lisp    : "$LISP_PATH

    echo hello world >/dev/null
}

################################################################################

ronin_setup_lisp () {
    echo hello world >/dev/null
}

